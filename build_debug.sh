#!/bin/bash

mkdir build
pushd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build .
popd
