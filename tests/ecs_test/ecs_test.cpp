#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <cstdint>
#include <string>
#include <iostream>
#include <vector>
#include "dagmar/Coordinator.h"

dag::Coordinator ecsCoordinator;

struct A
{
    float x = 0;
};

struct B
{
    float y = 0;
};

TEST_CASE("Coordinator Component Registration", "[component_registration]") {
    
    ecsCoordinator.startup();

    ecsCoordinator.registerComponent<A>();

    dag::ComponentType compABits = ecsCoordinator.getComponentType<A>();

    REQUIRE(compABits == 0);

    ecsCoordinator.registerComponent<B>();

    dag::ComponentType compBBits = ecsCoordinator.getComponentType<B>();

    REQUIRE(compBBits == 1);

    ecsCoordinator.shutdown();
}

class EmptySys : public dag::System
{
    public:
        void foo() {};
};

class FilledSys : public dag::System
{
    public:
        void bar() {};
};

TEST_CASE("Coordinator System Registration", "[system_registration]") {
    
    ecsCoordinator.startup();

    ecsCoordinator.registerComponent<A>();
    ecsCoordinator.registerComponent<B>();

    // First empty system
    ecsCoordinator.registerSystem<EmptySys>();
    dag::Signature emptySysSig;

    ecsCoordinator.setSystemSignature<EmptySys>(emptySysSig);
    REQUIRE(ecsCoordinator.getSystemSignature<EmptySys>() == 0);

    // Second system
    auto fs = ecsCoordinator.registerSystem<FilledSys>();
    dag::Signature filledSysSig;

    filledSysSig.set(ecsCoordinator.getComponentType<A>());
    ecsCoordinator.setSystemSignature<FilledSys>(filledSysSig);
    REQUIRE(ecsCoordinator.getSystemSignature<FilledSys>() == 1);
    filledSysSig.set(ecsCoordinator.getComponentType<B>());
    ecsCoordinator.setSystemSignature<FilledSys>(filledSysSig);
    REQUIRE(ecsCoordinator.getSystemSignature<FilledSys>() == 3);

    // second system iteration to check if they are correctly registered
    
    std::vector<dag::Entity> entities(3);
    entities[0] = ecsCoordinator.createEntity();
    ecsCoordinator.addComponent(entities[0], A{});

    entities[1] = ecsCoordinator.createEntity();
    ecsCoordinator.addComponent(entities[1], B{});

    entities[2] = ecsCoordinator.createEntity();
    ecsCoordinator.addComponent(entities[2], A{});
    ecsCoordinator.addComponent(entities[2], B{});

    // Only the entities[2] should be added to this system
    REQUIRE(fs->entities.size() == 1);
    REQUIRE(*(fs->entities.begin()) == entities[2]);

    // Removing test
    ecsCoordinator.removeComponent<A>(entities[2]);
    REQUIRE(fs->entities.size() == 0);

    // Adding the component to another entity
    ecsCoordinator.addComponent(entities[0], B{});
    REQUIRE(fs->entities.size() == 1);
    REQUIRE(*(fs->entities.begin()) == entities[0]);

    // Remove entity
    ecsCoordinator.destroyEntity(entities[0]);
    REQUIRE(fs->entities.size() == 0);

    ecsCoordinator.shutdown();
}

using Catch::Matchers::Contains;

TEST_CASE("ECS Exceptions: Insert Existing Component", "[insert_component_already_existent]") {
    
    ecsCoordinator.startup();
    ecsCoordinator.registerComponent<A>();

    auto entity = ecsCoordinator.createEntity();
    ecsCoordinator.addComponent(entity, A{});
    
    REQUIRE_THROWS_WITH(ecsCoordinator.addComponent(entity, A{}), Contains("Insert component failed: Component already added to the entity."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Remove Component Not Existent", "[remove_component_not_existent]") {
    
    ecsCoordinator.startup();
    ecsCoordinator.registerComponent<A>();

    auto entity = ecsCoordinator.createEntity();
    REQUIRE_THROWS_WITH(ecsCoordinator.removeComponent<A>(entity), Contains("Remove component failed: Component does not exist."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Get Component Not Existent", "[get_component_not_existent]") {
    
    ecsCoordinator.startup();
    ecsCoordinator.registerComponent<A>();

    auto entity = ecsCoordinator.createEntity();

    REQUIRE_THROWS_WITH(ecsCoordinator.getComponent<A>(entity), Contains("Get component failed: Component does not exist"));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Register Existing Component", "[register_component_already_existing]") {
    
    ecsCoordinator.startup();

    ecsCoordinator.registerComponent<A>();
    REQUIRE_THROWS_WITH(ecsCoordinator.registerComponent<A>(), Contains("Register component failed: Component already registered."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Get Component Type Not Existent", "[get_component_type_not_existent]") {    
    ecsCoordinator.startup();

    ecsCoordinator.registerComponent<A>();
    auto entity = ecsCoordinator.createEntity();
    REQUIRE_THROWS_WITH(ecsCoordinator.getComponentType<B>(), Contains("Get component type failed: Component does not exist."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Get Component Array Not Existent", "[get_component_array_not_existent]") {
    
    ecsCoordinator.startup();

    auto entity = ecsCoordinator.createEntity();
    REQUIRE_THROWS_WITH(ecsCoordinator.addComponent(entity, A{}), Contains("Get component array failed: Component array does not exist."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Register System Already Existent", "[register_system_already_existent]") {
    
    ecsCoordinator.startup();
    ecsCoordinator.registerSystem<EmptySys>();
    REQUIRE_THROWS_WITH(ecsCoordinator.registerSystem<EmptySys>(), Contains("Register system failed: System already existent."));
    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Set System Signature Not Existent", "[set_system_signature_not_existent]") {
    
    ecsCoordinator.startup();
    ecsCoordinator.registerComponent<A>();

    dag::Signature sysSig;
    sysSig.set(ecsCoordinator.getComponentType<A>());

    REQUIRE_THROWS_WITH(ecsCoordinator.setSystemSignature<EmptySys>(sysSig), Contains("Set system signature failed: System does not exist."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Get System Signature Not Existent", "[get_system_signature_not_existent]") {
    ecsCoordinator.startup();

    REQUIRE_THROWS_WITH(ecsCoordinator.getSystemSignature<EmptySys>(), Contains("Get system signature failed: System does not exist."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Create Too Many Entities", "[create_too_many_entities]") {
    
    ecsCoordinator.startup();

    for(size_t i = 0; i < dag::MAX_ENTITIES; i++)
    {
        ecsCoordinator.createEntity();
    }

    REQUIRE_THROWS_WITH(ecsCoordinator.createEntity(), Contains("Create entity failed: MAX_ENTITIES limit has been reached."));

    ecsCoordinator.shutdown();
}

TEST_CASE("ECS Exceptions: Destroy entity out of range", "[destroy_entity_out_of_range]") {
    
    ecsCoordinator.startup();

    REQUIRE_THROWS_WITH(ecsCoordinator.destroyEntity(dag::MAX_ENTITIES), Contains("Destroy entity failed: Entity out of range."));

    ecsCoordinator.shutdown();
}