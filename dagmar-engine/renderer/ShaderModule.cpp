#include <GL/glew.h>
#include <string>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <limits>

#include "dagmar/ShaderModule.h"
#include "dagmar/Log.h"
#include "dagmar/Exceptions.h"

/**
 * @brief Shader module constructor
 */
dag::ShaderModule::ShaderModule(std::filesystem::path const& shaderPath, int const& shaderType) : type(shaderType)
{
    setCodeAndCompile(shaderPath, shaderType);
}

/**
 * @brief Shader module destructor
 */
dag::ShaderModule::~ShaderModule()
{ 
    glDeleteShader(id);
}

/**
 * @brief Compiles the shader module
 */
void dag::ShaderModule::setCodeAndCompile(std::filesystem::path const& shaderPath, int const& shaderType)
{
    Log::debug("Attempting to create shader from: " + shaderPath.string());
    std::string code;
    std::ifstream shaderFile;
    // ensure ifstream objects can throw exceptions:
    shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        // open files
        shaderFile.open(shaderPath);
        Log::debug("Vertex shader file successfully opened");
        std::stringstream shaderStream;

        // read file's buffer contents into streams
        shaderStream << shaderFile.rdbuf();

        // close file handlers
        shaderFile.close();

        // convert stream into string
        code = shaderStream.str();
    }
    catch (std::ifstream::failure e)
    {
        Log::error("SHADER::FILE_NOT_SUCCESFULLY_READ");
        Log::error(e.what());
        throw std::runtime_error("error");
    }

    const char * shader_code = code.c_str();

    int success;
    char infoLog[512];

    id = glCreateShader(shaderType);

    glShaderSource(id, 1, &shader_code, NULL);
    glCompileShader(id);
    // print compile errors if any and throw
    glGetShaderiv(id, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(id, 512, NULL, infoLog);
        Log::error("SHADER::COMPILATION_FAILED:");
        Log::error(infoLog);
        throw dag::ShaderCompilationException(shaderType);
    }
}

bool dag::ShaderModule::operator==(dag::ShaderModule const& other) const
{
    if (id == other.id && type == other.type)
    {
        return true;
    }

    return false;
}

bool dag::ShaderModule::operator!=(dag::ShaderModule const& other) const
{
    if (*this == other)
    {
        return false;
    }

    return true;
}
