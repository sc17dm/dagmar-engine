#pragma once

#include <memory>

#include "dagmar/IndexBuffer.h"
#include "dagmar/VertexBuffer.h"

namespace dag
{
    /**
     * @brief VertexArrayObject
     *
     * Responsible for composing a VAO.
     * @tparam T
     * @tparam V
    */
    template <class T, class V>
    class VertexArrayObject
    {
        public:
            uint32_t id = 0;
            std::shared_ptr<VertexBuffer<T>> vertexBuffer;
            std::shared_ptr<IndexBuffer<V>> indexBuffer;

        public:
            // Default constructor
            VertexArrayObject<T, V>(std::shared_ptr<VertexBuffer<T>> const& vertexBuffer, std::shared_ptr<IndexBuffer<V>> const& indexBuffer)
            {
                this->vertexBuffer = vertexBuffer;
                this->indexBuffer = indexBuffer;
            }

            ~VertexArrayObject<T, V>()
            {
                if (id != 0)
                {
                    glDeleteVertexArrays(1, &id);
                }
            }

            // Generate method for VAO
            void generate()
            {
                if (id == 0)
                {
                    glGenVertexArrays(1, &id);
                }
            }

            // Combines the vertex & index buffers
            void aggregate(int32_t const& mode = GL_TRIANGLES)
            {
                if (id != 0)
                {
                    bind();

                    vertexBuffer->bind();
                    vertexBuffer->load(mode);
                    vertexBuffer->activateAttributes();

                    indexBuffer->bind();
                    indexBuffer->load(mode);

                    unbind();

                    indexBuffer->unbind();
                    vertexBuffer->unbind();
                }
            }

            // Bind method for VAO
            void bind()
            {
                glBindVertexArray(id);
            }

            // Unbind method for VAO
            void unbind()
            {
                glBindVertexArray(0);
            }

            // Draw method for VAO (dependent on what buffers we have)
            void draw(int32_t const& mode = GL_TRIANGLES)
            {
                bind();

                if(indexBuffer && vertexBuffer)
                {
                    indexBuffer->draw(mode);
                }
                else if (vertexBuffer)
                {
                    vertexBuffer->draw(mode);
                }
                
                unbind();
            }
    };
}
