#pragma once

#include <GL/glew.h>
#include <string>
#include <filesystem>
#include <vector>

#include "dagmar/ShaderModule.h"

namespace dag
{
    /**
     * @class Shader program 
     */
    class ShaderProgram
    {
        public:
            unsigned int id;

            ShaderProgram(std::vector<std::shared_ptr<dag::ShaderModule>>& shaders);
            ~ShaderProgram();

            // tell gl to use this program
            void use();

            // get the current uniform location
            int getUniformLoc(const std::string& name) const;
    };
}
