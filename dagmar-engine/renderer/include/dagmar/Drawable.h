#pragma once

namespace dag
{
	/**
	 * @Drawable object interface
	 *
	 * Enforces the implementation of a void draw() class.
	 *
	 */
	class Drawable
	{
	public:
		// Default constructor
		Drawable() = default;

		// Virtual destructor
		virtual ~Drawable() = default;

		// draw method must be implemented by child classes.
		// it is intended to be used for drawing data to opengl
		virtual void draw() const = 0;
	};
}