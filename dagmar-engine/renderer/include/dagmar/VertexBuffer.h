#pragma once

#include "Buffer.h"

#include <cstdint>
#include <GL/glew.h>

#include "dagmar/VertexAttribute.h"

namespace dag
{
    /**
     * @brief Vertex Buffer Class
     */
    template <class T>
    class VertexBuffer : public Buffer<T>
    {
        private:
            std::vector<VertexAttribute> attributes;

        public:
            // Old-style constructor with no attributes
            VertexBuffer(void* data, uint32_t const& size) : Buffer<T>(data, size) { }

            // Old style constructor with attributes
            VertexBuffer(void* data, uint32_t const& size, void* attributes, uint32_t const& attrSize) : Buffer<T>(data, size)
            {
                for (uint32_t i = 0; i < attrSize; i++)
	            {           
                    this->attributes.push_back(static_cast<T*>(attributes)[i]);
                }
            }

            // Vector-based constructor with no attributes
            VertexBuffer(std::vector<T> const& data) : Buffer<T>(data) { }

            // Vector-based constructor with attributes
            VertexBuffer(std::vector<T> const& data, std::vector<VertexAttribute> const& attributes) : Buffer<T>(data)
            {
                this->attributes = attributes;
            }

            // Copy constructor
            VertexBuffer(VertexBuffer const& other) : Buffer<T>(other)
            {
                this->attributes = other.attributes;
            }

            // Virtual function to update the buffer
            virtual void update(void* data, uint32_t const& offset, bool const& flushToGPU)
            {
                if (flushToGPU)
                {
                    bind();
                    glBufferSubData(GL_ARRAY_BUFFER, sizeof(T) * offset, sizeof(T), data);
                    unbind();
                }

                this->elements[offset] = *(reinterpret_cast<T*>(data));
            }

            // Virtual function to map the buffer
            virtual void* map(int32_t const& access)
            {
                this->mapping = glMapBuffer(GL_ARRAY_BUFFER, access);
                return this->mapping;
            }

            // Virtual function to map range
            virtual void* mapRange(uint32_t const& offset, uint32_t const& length, int32_t const& access)
            {
                this->mapping = glMapBufferRange(GL_ARRAY_BUFFER, offset, length, access);
                return this->mapping;
            }

            // Virtual function to unmap the buffer
            virtual void unmap()
            {
                glUnmapBuffer(GL_ARRAY_BUFFER);
            }

            // Virtual function to load the buffer
            virtual void load(int32_t const& usage)
            {
                glBufferData(GL_ARRAY_BUFFER, this->elements.size() * sizeof(T), this->elements.data(), usage);
            }

            // Virtual fucntion to bind the buffer
            virtual void bind()
            {
                glBindBuffer(GL_ARRAY_BUFFER, this->id);
            }

            // Virtual function to unbind the buffer
            virtual void unbind()
            {
                glBindBuffer(GL_ARRAY_BUFFER, 0);
            }

            // Virtual draw method for vertex buffers
            virtual void draw(int32_t const& mode)
            {
                glDrawArrays(mode, 0, static_cast<uint32_t>(this->elements.size()));
            }

            // Method to add a vertex attribute
            void addAttribute(VertexAttribute const& va)
            {
                attributes.push_back(va);
            }

            // Method to activate all the vertex attributes
            void activateAttributes()
            {
                for (auto& attribute : attributes)
                {
                    attribute.activate();
                }
            }
    };
}
