#include <GL/glew.h>
#include <string>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <limits.h>
#include <dagmar/Log.h>

#include "dagmar/ShaderProgram.h"
#include "dagmar/ShaderModule.h"
#include "dagmar/Exceptions.h"

/**
 * @brief Constructor for the shader program
 *
 * Given a list of shaders, it constructs a valid shader pipeline
 */
dag::ShaderProgram::ShaderProgram(std::vector<std::shared_ptr<dag::ShaderModule>> & shaders) : id(UINT_MAX)
{
    for (auto shader0 : shaders) {
        for (auto shader1 : shaders) {
            if ((shader0 != shader1) && (shader0->type == shader1->type))
            {
                Log::warning("Trying to attach multiple shaders of the same type to a shader program!");
            }
        }
    }

    id = glCreateProgram();

    for (auto const & shader : shaders) {
        glAttachShader(id, shader->id);
    }

    int num_shaders;

    glGetProgramiv(id, GL_ATTACHED_SHADERS, &num_shaders);

    int success;
    char infoLog[1024];

    glLinkProgram(id);
    // print linking errors if any
    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(id, 1024, NULL, infoLog);
        Log::error("SHADER::PROGRAM::LINKING_FAILED");
        Log::error(infoLog);
        throw dag::ShaderProgramLinkingException();
    }
}

/**
 * @brief Shader Program destructor
 */
dag::ShaderProgram::~ShaderProgram()
{
    glDeleteProgram(id);
}

/**
 * @brief Uses the shader
 */
void dag::ShaderProgram::use()
{
    glUseProgram(id);
}

/**
 * @brief Gets uniform location
 */
int dag::ShaderProgram::getUniformLoc(std::string const& name) const
{
    return glGetUniformLocation(id, name.c_str());
}
