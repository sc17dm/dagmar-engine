#pragma once

#include <set>
#include "Entity.h"

namespace dag
{
    /**
     * @brief Simple system class
     */
    class System
    {
        public:
            std::set<Entity> entities;
    };
}
