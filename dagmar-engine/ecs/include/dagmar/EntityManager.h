#pragma once

#include <queue>
#include <array>

#include "Entity.h"
#include "Component.h"

namespace dag
{
    /**
     * @brief Entity manager class
     */
    class EntityManager
    {
        private:
            std::queue<Entity> availableEntities;
            std::array<Signature, MAX_ENTITIES> signatures;
            uint32_t aliveEntities;
    
        public:
            // Default constructor
            EntityManager();
    
            // Creates an entity and returns its id
            Entity createEntity();
    
            // Destroys an entity
            void destroyEntity(Entity const& entity);
    
            // Sets the signature of an entity
            void setSignature(Entity const& entity, Signature const& signature);
    
            // Gets the signature of an existing entity
            Signature const getSignature(Entity const& entity) const;
    };
}
