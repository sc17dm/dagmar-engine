#pragma once

#include <unordered_map>
#include <memory>
#include <cassert>
#include <typeindex>

#include "dagmar/Exceptions.h"

#include "Entity.h"
#include "Component.h"
#include "System.h"

namespace dag
{
    /**
     * @brief System Manager class
     * Manages the systems and their signatures
     */
    class SystemManager
    {
        private:
            std::unordered_map<std::type_index, Signature> signatures;
            std::unordered_map<std::type_index, std::shared_ptr<System>> systems;
    
        public:
            /**
             * @brief Tries to register a system
             */
            template <typename T>
            std::shared_ptr<T> registerSystem()
            {
				std::type_index type = std::type_index(typeid(T));

                if(systems.contains(type))
                {
                    throw dag::ECSException(dag::ECSException::ErrorType::RegisterSystemAlreadyExistent);
                }
    
                auto system = std::make_shared<T>();
                systems.insert({type, system});
                return system;
            }
    
            /**
             * @brief Sets the signature of a system
             */
            template <typename T>
            void setSignature(Signature const& signature)
            {
				std::type_index type = std::type_index(typeid(T));

                if(!systems.contains(type))
                {
                    throw dag::ECSException(dag::ECSException::ErrorType::SetSystemSignatureNotExistent);
                }

                signatures[type] = signature;
            }

            /**
             * @brief Gets the signature of a system
             */
            template <typename T>
            Signature const getSignature()
            {
				std::type_index type = std::type_index(typeid(T));

                if(!systems.contains(type))
                {
                    throw dag::ECSException(dag::ECSException::ErrorType::GetSystemSignatureNotExistent);
                }

                return signatures[type];
            }
    
            // Erases a destroyed entity from all the systems
            void entityDestroyed(Entity const& entity);
    
            // Signals the systems that an entity's signature has changed
            void entitySignatureChanged(Entity const& entity, Signature const& signature);
    };
}
