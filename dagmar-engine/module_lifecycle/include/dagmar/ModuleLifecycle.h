#pragma once
#include "dagmar/Module.h"
#include "dagmar/PlatformDetector.h"
#include "dagmar/Log.h"
#include "dagmar/Coordinator.h"
#include <vector>
#include <memory>

namespace dag
{
	/**
	 * @brief Class to manage the dependancies between modules
	 */
	class ModuleLifecycle
	{
	public:
		// ALL Modules should be listed here IN ORDER of initialisation
		std::vector<std::shared_ptr<Module>> modules =
		{
			platformDetector,
            coordinator
		};

		// This class is a singleton, this is how it should be retrieved.
		static ModuleLifecycle& get();

		// function to start up all modules in the desired order
		void startup();

		// function to shut down all modules in the desired order (roughly the reverse of the start up)
		void shutdown();

	private:
		// Default [con/de]structors
		ModuleLifecycle() = default;
		~ModuleLifecycle() = default;
	};
}
