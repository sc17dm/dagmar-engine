#pragma once
#include <string>
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#endif

/**
 * @brief Log class
 *
 * Collection of static functions to aid in printing text in the console.
 */
class Log
{
    public:
        // Log Type that can be used
        enum class Type
        {
            Debug,
            Info,
            Warning,
            Error,
            Success
        };

#ifdef _WIN32
        #define RED "\033[31m"
        #define GREEN "\033[32m"
        #define YELLOW "\033[33m"
        #define MAGENTA "\033[35m"
        #define CYAN "\033[36m"
        #define RESET "\033[0m"
#else
        #define RED "\e[0;31m"
        #define GREEN "\e[0;32m"
        #define YELLOW "\e[0;33m"
        #define MAGENTA "\e[0;35m"
        #define CYAN "\e[0;36m"
        #define RESET "\033[0m"
#endif

        /**
         * @brief Print log
         * @param text
         */
        template <typename T>
        static void print(T const & text, Type const & logType)
        {
            switch (logType)
            {
            case Type::Debug:
            {
                debug(text);
                break;
            }
            case Type::Info:
            {
                info(text);
                break;
            }
            case Type::Warning:
            {
                warning(text);
                break;
            }
            case Type::Error:
            {
                error(text);
                break;
            }
            case Type::Success:
            {
                success(text);
                break;
            }
            default:
            {
                break;
            }
            };
        }

        /**
         * @brief Debug log
         * @param text
         */
        template<typename T>
        static void debug(T const & input)
        {
            std::cout << MAGENTA "[DEBUG]: " << input << RESET << std::endl;
        }
        /**
         * @brief Info log
         * @param text
         */
        template <typename T>
        static void info(T const & text)
        {
            std::cout << CYAN "[INFO]: " << text << RESET << std::endl;
        }

        /**
         * @brief Warning log
         * @param text
         */
        template <typename T>
        static void warning(T const & text)
        {
            std::cout << YELLOW "[WARNING]: " << text << RESET << std::endl;
        }

        /**
         * @brief Error log
         * @param text
         */
        template <typename T>
        static void error(T const & text)
        {
            std::cout << RED "[ERROR]: " << text << RESET << std::endl;
        }

        /**
         * @brief Success log
         * @param text
         */
        template <typename T>
        static void success(T const & text)
        {
            std::cout << GREEN "[SUCCESS]: " << text << RESET << std::endl;
        }
};
