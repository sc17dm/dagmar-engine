// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <vector>
#include <string>
using namespace glm;

#include "dagmar/FileSystem.h"
#include "dagmar/Log.h"
#include "dagmar/Drawable.h"
#include "dagmar/ShaderProgram.h"
#include "dagmar/ShaderModule.h"
#include "dagmar/ModuleLifecycle.h"

#include "dagmar/VertexArrayObject.h"

const std::string MODEL_NAME = "duck.obj";
const std::string TEXTURE_NAME = "duck.jpg";

namespace dag
{
	// THIS WILL BE REPLACED AT SOME POINT BY SOMETHING FAR BETTER
	// for now it is a basic _thing_ that inherits from drawable for me ot test
	class TestModelClass : public dag::Drawable
	{
	public:
		TestModelClass(std::vector<std::shared_ptr<dag::ShaderModule>> shaders) : shader_prog(shaders)
		{
            // Buffers example
            auto vb = std::make_shared<VertexBuffer<float>>(vertices);
            vb->generate();
            vb->addAttribute(VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0));

            auto ib = std::make_shared<IndexBuffer<uint32_t>>(indices);
            ib->generate();

            vao = std::make_shared<VertexArrayObject<float, uint32_t>>(vb, ib);
            vao->generate();

            vao->aggregate(GL_STATIC_DRAW);
		}

		// the thing i actually wanted to test :))))
		void draw() const
		{
			glUseProgram(shader_prog.id);
            vao->draw(GL_TRIANGLES);
		}

	private:
		dag::ShaderProgram shader_prog;

        std::shared_ptr<VertexArrayObject<float, uint32_t>> vao;

		std::vector<float> vertices =
        {
			0.5f, 0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			-0.5f, -0.5f, 0.0f,
			-0.5f, 0.5f, 0.0f
		};

		std::vector<unsigned int> indices =
        {
			0, 1, 3,
			1, 2, 3
		};
	};
}

int main(void)
{
	dag::ModuleLifecycle::get().startup();

	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		getchar();
		return -1;
	}

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, (dag::filesystem::getAssetPath()/MODEL_NAME).string().c_str())) {
		std::cerr << warn + err;
	}

	Log::debug(std::string("Loaded model: ") + std::to_string(shapes[0].mesh.indices.size()) + std::string(" indices"));

	int texWidth, texHeight, texChannels;

    stbi_uc* pixels = stbi_load((dag::filesystem::getAssetPath()/TEXTURE_NAME).string().c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    size_t imageSize = texWidth * texHeight * 4;

    if (!pixels) {
        throw std::runtime_error("failed to load the texture image");
    }

	Log::debug(std::string("Loaded texture: (") + std::to_string(texWidth) + " x " + std::to_string(texHeight) + " x " + std::to_string(texChannels) + std::string(")"));



	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(1024, 768, "Tutorial 01", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialise imgui helper functions
	IMGUI_CHECKVERSION();
    ImGui::CreateContext();
	const char* glsl_version = "#version 130";
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);
	
	// Initialize GLEW
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(1.0f, 0.5f, 0.45f, 0.0f);
	
    Log::error("error test");
    Log::warning("warning test");
    Log::success("success test");
    Log::info("info test");
    Log::debug("debug test");

	std::vector<std::shared_ptr<dag::ShaderModule>> shaders = { std::make_shared<dag::ShaderModule>(dag::filesystem::getAssetPath() / "shaders/glsl.vert", GL_VERTEX_SHADER),
															   std::make_shared<dag::ShaderModule>(dag::filesystem::getAssetPath() / "shaders/glsl.frag", GL_FRAGMENT_SHADER) };

	dag::TestModelClass tmc(shaders);

	 // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		!glfwWindowShouldClose(window)){
		// Clear the screen. It's not mentioned before Tutorial 02, but it can cause flickering, so it's there nonetheless.
		glClear(GL_COLOR_BUFFER_BIT);

		glfwPollEvents();

		// new frame from helper functions
		ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
		// demo window from imgui
		ImGui::ShowDemoWindow();

		ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(1.0f, 0.5f, 0.45f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);

		tmc.draw();

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		glfwSwapBuffers(window);
	}
	
	ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

	// Close OpenGL window and terminate GLFW
	glfwDestroyWindow(window);
	glfwTerminate();

	dag::ModuleLifecycle::get().shutdown();

	return 0;
}
