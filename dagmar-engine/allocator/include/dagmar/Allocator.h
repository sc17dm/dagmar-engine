#pragma once
#include <iostream>
#include <new>
#include <memory>

#include "dagmar/Log.h"

using word_t = intptr_t;

namespace dag
{
    /**
     * @brief Custom memory allocator
     */
    class Allocator
    {
        // Block structure to use for memory blocks.
        struct Block
        {
            // Header that encodes size and used bit flag
            word_t header;
            Block* next = nullptr;
            Block* prev = nullptr;
            void* data;
        };

        private:
            // Memory pointer for allocate and free from/to OS
            void* memory;
            // The starting block for the double-linked list
            Block* start;

        public:
            // Constructor with initial memory size - big block
            Allocator(word_t const& initialSize);
            // Destructor to free the memory
            ~Allocator();

            // Allocate from big block
            void* allocate(word_t const& size);
            // Free a block, return it to big block
            void deallocate(void* block);
            // Debug printing
            void debugPrint();
            // Align a given size to word size
            inline word_t align(word_t const& size)
            {
                return size + (sizeof(word_t) - 1) & ~(sizeof(word_t) - 1);
            }

        private:
            // Combine memory block with neighbours
            void coalesce(Block* block);
            // Break a bigger block into a size block and a remaining block
            Block* breakBlock(Block* block, word_t const& size);
            // Get the size of the header
            word_t getHeaderSize();

            // Get size from header, removing used bit flag
            inline size_t getSize(Block* block)
            {
                return block->header & ~1L;
            }
            // Get value of used bit flag
            inline bool isUsed(Block* block)
            {
                return block->header & 1;
            }
            // Set value of used bit flag
            inline void setUsed(Block* block, bool const& used)
            {
                if (used)
                {
                    block->header |= 1;
                }
                else
                {
                    block->header &= ~1;
                }
            }
    };
}